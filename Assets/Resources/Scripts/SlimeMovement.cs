using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeMovement : MonoBehaviour
{
    float time = 3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (time <= 0)
        {
            Jump();
            time = 3;
        }
        time -= Time.deltaTime;
    }

    void Jump()
    {
        GetComponent<Animator>().SetTrigger("jump");
        float dir = Mathf.Sign(Random.Range(-10f, 10f));
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(dir, 2)*300);
    }
}
