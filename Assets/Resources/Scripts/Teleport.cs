using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Teleport : MonoBehaviour
{
    public GameObject Spawn;
    public GameObject CM;
    public Text LevelCount;

    [Header("Level scripts for reset")]
    public PlayerMovement playerMovement;
    public WheelBehaviour wheelBehaviour;
    public MalusSpawner malusSpawner;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.transform.position = Spawn.transform.position;
            Camera.main.gameObject.transform.position = Spawn.transform.position;
            CM.transform.position = Spawn.transform.position;
            LevelCount.text = GameManager.instance.LevelIterationCount + "";
            GameManager.instance.ChangeSkyColor(GameManager.instance.LevelIterationCount);
            ResetLevel();
        }
    }
    

    /// <summary>
    /// Reset all scripts in the level
    /// </summary>
    private void ResetLevel()
    {
        playerMovement.ResetLevel();
        wheelBehaviour.ResetLevel();
    }
}
