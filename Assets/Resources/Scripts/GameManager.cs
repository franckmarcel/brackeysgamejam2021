using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using System.Security.Cryptography;
using System.Text;

public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Instance of the game manager
    /// </summary>
    public static GameManager instance = null;

    public Stopwatch timer;

    public int LevelIterationCount = 0;

    public Camera camera;
    public int nightLevel = 7;
    public Color
        dawn, morning, noon, afternoon, twilight, evening, night;
    private Color[] dayCycle;

    /// <summary>
    /// Awake.
    /// </summary>
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {
            //if not, set instance to this
            instance = this;
        }

        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Start
    /// </summary>
    void Start()
    {
        timer = new Stopwatch();
        timer.Start();
        dayCycle = new Color[] { dawn, morning, noon, afternoon, twilight, evening, night };
        ChangeSkyColor(0);
    }

    /// <summary>
    /// Update
    /// </summary>
    void Update()
    {

    }

    public string ChronoFormat()
    {
        TimeSpan time = timer.Elapsed;

        return string.Format("{0:D2}:{1:D2}:{2:D3}",
                        time.Minutes,
                        time.Seconds,
                        time.Milliseconds);
    }

    public void ChangeSkyColor(int level)
    {
        float index = nightLevel / (dayCycle.Length + 0.0f) * level;
        camera.backgroundColor = level > nightLevel ? night : dayCycle[(int)index];
    }
}
