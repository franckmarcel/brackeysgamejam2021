using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuTitle : MonoBehaviour
{
    float x, y;

    // Start is called before the first frame update
    void Start()
    {
        x = gameObject.transform.localPosition.x;
        y = gameObject.transform.localPosition.y;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        gameObject.transform.Translate(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), 0);
        gameObject.transform.Rotate(0, 0, Random.Range(-1f, 1f));
        float xClamp = Mathf.Clamp(gameObject.transform.localPosition.x, x - 20, x + 20);
        float yClamp = Mathf.Clamp(gameObject.transform.localPosition.y, y - 5, y + 5);
        gameObject.transform.localPosition = new Vector2(xClamp, yClamp);
    }



    

}
