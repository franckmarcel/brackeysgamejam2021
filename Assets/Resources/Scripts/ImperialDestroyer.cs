using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImperialDestroyer : MonoBehaviour
{

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Porkchop"))
        {
            Destroy(collision.gameObject);
        }
    }

}
