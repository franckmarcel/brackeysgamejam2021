using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestTrigger : MonoBehaviour
{

    public int heartLimit = 5;
    public int maxHeart = 10;
    
    float percent;
    float offset;
    float heartNumber = 0;
    bool canOpen = true;

    // Start is called before the first frame update
    void Start()
    {
        ResetPercent();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && canOpen)
        {
            int random = Random.Range(0, (int)(100f / percent));
            if (random == 0)
            {
                gameObject.GetComponent<SpriteRenderer>().color = Color.red;
                ResetPercent();
            }
            else
            {
                GameObject heart = Instantiate(Resources.Load<GameObject>("Prefabs/Heart"), gameObject.transform.position, Quaternion.identity);
                Vector2 direction = new Vector2(0, 200);
                heart.GetComponent<Rigidbody2D>().AddForce(direction);
                ChangePercent();
            }
            canOpen = false;
        }
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }

    private void ResetPercent()
    {
        percent = 0;
        // change l'offset
        offset = 100 / maxHeart;
        if (maxHeart != heartLimit)
            maxHeart--;
    }


    private void ChangePercent()
    {
        if (heartNumber == maxHeart)
            ResetPercent();
        else
            percent += offset;
    }


    

}
