using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelBehaviour : MonoBehaviour
{

    bool IsSpinning = false;
    float interpolation = 0;

    public GameObject Needle;
    public GameObject Death;
    public GameObject TextBubble;

    public float initialSpeed = 1000f;
    public float Speed = 0.01f;
    public float decelerationSpeed = 10f;

    public MalusSpawner malusSpawner;

    private void Start()
    {
        Speed = initialSpeed;
    } 

    private void FixedUpdate()
    {
        if (IsSpinning)
        {
            if (Speed > 0)
            {
                interpolation += Time.deltaTime * Speed;
                Needle.transform.rotation = new Quaternion(0, 0, -interpolation, 0);
                //Vector3 newRot = Vector3.Lerp(Vector3.zero, new Vector3(0, 0, -500), interpolation);
                //Needle.transform.rotation = new Quaternion(newRot.x, newRot.y, newRot.z, 0);
                Needle.transform.RotateAround(Needle.transform.position, Vector3.forward, -interpolation);
                Speed -= decelerationSpeed;
            }
            else
            {
                //TODO Malus + death script
                Death.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/the_death_awaken");
                TextBubble.SetActive(true);

                //Increase level count
                GameManager.instance.LevelIterationCount++;
                IsSpinning = false;
                GetComponent<BoxCollider2D>().enabled = false;

                // Generate Malus
                malusSpawner.GenerateMalus();
                malusSpawner.SpawnMalus();
                malusSpawner.Respawn();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            IsSpinning = true;
        }
    }

    /// <summary>
    /// Reset the wheel of infortune
    /// </summary>
    public void ResetLevel()
    {
        Needle.transform.rotation = new Quaternion(0, 0, 0, 0);
        Death.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/the_death");
        IsSpinning = false;
        Speed = initialSpeed;
        GetComponent<BoxCollider2D>().enabled = true;
        TextBubble.SetActive(false);
    }
}
