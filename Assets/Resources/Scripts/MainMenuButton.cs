using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MainMenuButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Text>().color = Color.white;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        gameObject.GetComponent<Text>().color = new Color(1,1,1, Random.Range(0.2f, 0.8f));
    }
}
