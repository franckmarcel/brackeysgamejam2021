using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Move player in 2D space
    [Header("Physics")]
    public float maxSpeed = 5f;
    public float jumpHeight = 400f;
    public float gravityScale = 1f;
    public float projectileForce = 1000f;
    public LayerMask WhatIsGround;

    [Header("Character")]
    public GameObject arm;
    public  Transform GroundCheck;

    [Header("GameObjects")]
    public Camera mainCamera;
    public GameObject ProjectilesContainer;

    [Header("HUD")]
    public GameObject HealthContainer;
    
    [Header("Shakira")]
    public bool shake = true;
    public float rotation = 1f;
    public float shakeForce = 0.1f;

    float dx;
    float fartCoolDown;
    bool facingRight = true;
    float moveDirection = 0;
    bool isGrounded = false;
    Vector3 cameraPos;
    Vector3 groundCheckPos;
    Rigidbody2D r2d;
    BoxCollider2D mainCollider;
    Transform t;
    int JumpCount = 0;
    Vector3 point = new Vector3();
    int heart = 3;
    float countDown = 0;
    Animator animator;
    AudioSource audio;

    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        fartCoolDown = Random.Range(3f, 10f);
        t = transform;
        r2d = GetComponent<Rigidbody2D>();
        mainCollider = GetComponent<BoxCollider2D>();
        r2d.freezeRotation = true;
        r2d.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        r2d.gravityScale = gravityScale;
        facingRight = t.localScale.x > 0;

        animator = GetComponent<Animator>();

        if (mainCamera)
        {
            cameraPos = mainCamera.transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!animator.GetBool("Dead"))
        {
            groundCheckPos = GroundCheck.position;
            // Movement controls
            if ((Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.D))) //&& (isGrounded || Mathf.Abs(r2d.velocity.x) > 0.01f)
            {
                moveDirection = Input.GetKey(KeyCode.Q) ? -1 : 1;
            }
            else
            {
                if (isGrounded || r2d.velocity.magnitude < 0.01f)
                {
                    moveDirection = 0;
                }
            }

            // Change facing direction
            if (moveDirection != 0)
            {
                if (moveDirection > 0 && !facingRight)
                {
                    facingRight = true;
                    GetComponent<SpriteRenderer>().flipX = false;
                    arm.GetComponent<SpriteRenderer>().flipY = false;
                    arm.transform.localPosition = new Vector3(-0.22f, 0.33f, 0);
                    //t.localScale = new Vector3(Mathf.Abs(t.localScale.x), t.localScale.y, transform.localScale.z);
                }
                if (moveDirection < 0 && facingRight)
                {
                    facingRight = false;
                    GetComponent<SpriteRenderer>().flipX = true;
                    arm.GetComponent<SpriteRenderer>().flipY = true;
                    arm.transform.localPosition = new Vector3(0.22f, 0.33f, 0);
                    //t.localScale = new Vector3(-Mathf.Abs(t.localScale.x), t.localScale.y, t.localScale.z);
                }
            }

            // Throw projectile
            if (Input.GetMouseButtonDown(0))
            {
                GameObject Projectile = Instantiate(Resources.Load<GameObject>("Prefabs/Projectile"), gameObject.transform.position, Quaternion.identity);
                Vector2 direction = (point - gameObject.transform.position);
                Projectile.GetComponent<Rigidbody2D>().AddForce(direction * projectileForce);
                Projectile.transform.parent = ProjectilesContainer.transform;
            }

            // Jumping
            // Double jump
            if (Input.GetKeyDown(KeyCode.Space) && (isGrounded || JumpCount < 1))
            {
                // If double jumping
                if (!isGrounded)
                {
                    animator.SetTrigger("DoubleJump");
                }

                r2d.AddForce(new Vector2(r2d.velocity.x, jumpHeight));
                JumpCount++;
            }
            // Reduce jump when release space bar
            else if (Input.GetKeyUp(KeyCode.Space))
            {
                r2d.velocity = new Vector2(r2d.velocity.x, r2d.velocity.y * 0.5f);
            }

            // Camera follow
            if (mainCamera)
            {
                mainCamera.transform.position = new Vector3(t.position.x + dx, transform.position.y, cameraPos.z);
            }

            if (shake)
            {
                float dx = mainCamera.transform.position.x;
                float dy = mainCamera.transform.position.y;
                
                mainCamera.transform.Translate(Random.Range(-shakeForce, shakeForce), Random.Range(-shakeForce, shakeForce), 0);
                mainCamera.transform.Rotate(0, 0, Random.Range(-rotation, rotation));

                float xClamp = Mathf.Clamp(mainCamera.transform.localPosition.x, dx - 5f, dx + 5f);
                float yClamp = Mathf.Clamp(mainCamera.transform.localPosition.y, dy - 5f, dy + 5f);
                mainCamera.transform.localPosition = new Vector3(xClamp, yClamp, -10);
            }
            else
            {
                ResetCameraRotation();
            }
        }
    }

    void FixedUpdate()
    {
        Bounds colliderBounds = mainCollider.bounds;
        float colliderRadius = 0.1f;
        // Check if player is grounded
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheckPos, colliderRadius, WhatIsGround);
        //Check if any of the overlapping colliders are not player collider, if so, set isGrounded to true
        isGrounded = false;
        if (colliders.Length > 0)
        {
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject.tag != "Player")
                {
                    isGrounded = true;
                    JumpCount = 0;
                    break;
                }
            }
        }
        
        // Fart
        if (fartCoolDown <= 0)
        {
            Fart();
            Debug.Log("Fart is comming");
            r2d.AddForce(new Vector2(r2d.velocity.x, 100));
            fartCoolDown = Random.Range(3f, 10f);
        }

        animator.SetBool("Grounded", isGrounded);
        animator.SetFloat("Speed", Mathf.Abs(r2d.velocity.x));

        Vector2 dir = point - arm.transform.position;
        
        arm.transform.right = dir.normalized;

        // Apply movement velocity
        gameObject.transform.position = new Vector3(gameObject.transform.position.x + moveDirection * maxSpeed * Time.deltaTime, gameObject.transform.position.y);
        r2d.AddForce(new Vector2((moveDirection) * maxSpeed, 0));

        r2d.velocity = new Vector2(Mathf.Clamp(r2d.velocity.x, -maxSpeed, maxSpeed), r2d.velocity.y);

        // Simple debug
        Debug.DrawLine(groundCheckPos, groundCheckPos - new Vector3(0, colliderRadius, 0), isGrounded ? Color.green : Color.red);
        Debug.DrawLine(groundCheckPos, groundCheckPos - new Vector3(colliderRadius, 0, 0), isGrounded ? Color.green : Color.red);

        // The Final Count Down
        countDown -= Time.deltaTime;
        fartCoolDown -= Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Spike") && countDown <= 0)
        {
            TakeDamage();
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Spike") && countDown <= 0)
        {
            TakeDamage();
        } 
        else if (collision.gameObject.CompareTag("Heart") && heart < 3)
        {
            heart++;
        }
    }

    void TakeDamage()
    {
        if (!animator.GetBool("Dead"))
        {
            heart--;
            Destroy(HealthContainer.transform.GetChild(0).gameObject);

            if (heart <= 0)
            {
                //gameObject.GetComponent<SpriteRenderer>().color = Color.red;
                arm.SetActive(false);
                animator.SetBool("Dead", true);
                animator.SetTrigger("Hit");
                r2d.freezeRotation = false;
            }
            else
            {
                countDown = 2;
                animator.SetTrigger("Hit");
                StartCoroutine(Invincible());
            }
        }
    }

    IEnumerator Invincible()
    {
        while (countDown > 0)
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
            arm.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
            yield return new WaitForSeconds(0.3f);
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            arm.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            yield return new WaitForSeconds(0.3f);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sceneView"></param>
    void OnGUI()
    {
        Event currentEvent = Event.current;
        Vector2 mousePos = new Vector2();
        Camera cam = Camera.main;
        
        // Get the mouse position from Event.
        // Note that the y position from Event is inverted.
        mousePos.x = currentEvent.mousePosition.x;
        mousePos.y = Camera.main.pixelHeight - currentEvent.mousePosition.y;

        point = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 0));
    }

    /// <summary>
    /// Change color of player
    /// </summary>
    /// <param name="color"></param>
    public IEnumerator ChangeColor(Color color)
    {
        float t = 0;
        
        while(t < 1)
        {
            GetComponent<SpriteRenderer>().color = Color.Lerp(GetComponent<SpriteRenderer>().color, color, t);
            arm.GetComponent<SpriteRenderer>().color = Color.Lerp(arm.GetComponent<SpriteRenderer>().color, color, t);
            yield return new WaitForSeconds(0.1f);
            t += 0.1f;
        }
    }

    /// <summary>
    /// Reset the projectiles on the map
    /// </summary>
    public void ResetLevel()
    {
        for(int i = 0; i < ProjectilesContainer.transform.childCount; i++)
        {
            Destroy(ProjectilesContainer.transform.GetChild(i).gameObject);
        }
    }

    /// <summary>
    /// reset camera rotation
    /// </summary>
    public void ResetCameraRotation()
    {
        mainCamera.transform.rotation = Quaternion.identity;
    }

    public void ReverseCamera()
    {
        mainCamera.transform.rotation = Quaternion.AngleAxis(180, new Vector3(0,0,1));
    }

    public void Zoom()
    {
        mainCamera.orthographicSize = 3;
    }

    public void ResetZoom()
    {
        mainCamera.orthographicSize = 8.23f;
    }

    public void Fart()
    {
        audio.clip = Resources.Load<AudioClip>("Sounds/Fart");
        audio.Play();
    }

}
