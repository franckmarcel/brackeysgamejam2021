using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigMovement : MonoBehaviour
{
    [Header("Explosion")]
    public float force = 50;
    public float radius = 5;
    public float upliftModifer = 5;

    [Header("Check")]
    public LayerMask playerMask;
    public LayerMask wallMask;
    public Transform groundCheck;
    
    [Header("Move")]
    public float speed = 0.1f;

    bool isRunning = false;
    float explosionCountDown = 0f;
    Vector2 direction = Vector2.right;
    Vector3 groundCheckPos;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void Update()
    {
        groundCheckPos = groundCheck.position;
    }

    private void FixedUpdate()
    {
        RaycastHit2D hitPlayer = Physics2D.Raycast(transform.position, direction, 3, playerMask);
        RaycastHit2D hitWall = Physics2D.Raycast(transform.position, direction, 3, wallMask);
        Debug.DrawRay(gameObject.transform.position, direction*3);

        float interpolation = Time.deltaTime * speed;
        // If pig see the player
        if (hitPlayer.collider != null)
        {
            Follow(interpolation);
            if (!isRunning)
            {
                explosionCountDown = 3;
                isRunning = true;
            }
        }

        // If pig see wall
        if (hitWall.collider != null && !isRunning)
        {
            ChangeDir();
        }

        explosionCountDown -= Time.deltaTime;
        if (isRunning && explosionCountDown < 0)
            DoExplosion(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 1));

        if (!isRunning)
            Search(interpolation);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheckPos, 0.1f, wallMask);
        if (colliders.Length == 0 && !isRunning)
            ChangeDir();
    }  

    void Search(float t)
    {
        gameObject.transform.Translate(direction * t / 2);
    }

    void Follow(float t)
    {
        gameObject.transform.Translate(direction * t);
    }

    void ChangeDir()
    {
        direction = direction == Vector2.left ? Vector2.right : Vector2.left;
        gameObject.transform.localScale = new Vector3(-gameObject.transform.localScale.x, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
    }

    /// <summary>
    /// create an explosion force
    /// </summary>
    /// <param name="position">location of the explosion</param>
    public void DoExplosion(Vector3 position)
    {
        SummonPorkchop(gameObject.transform.position);
        transform.localPosition = position;
        StartCoroutine(WaitAndExplode());
        Destroy(gameObject);
    }

    void SummonPorkchop(Vector2 position)
    {
        int rdm = Random.Range(3, 7);
        for (int i = 0; i < rdm; i++)
        {
            float dx = Random.Range(-1f, 1f);
            float dy = Random.Range(0f, 2f);
            Instantiate(Resources.Load<GameObject>("Prefabs/Porkchop"), new Vector2(position.x + dx, position.y + dy), Quaternion.identity);
        }
    }

    /// <summary>
    /// exerts an explosion force on all rigidbodies within the given radius
    /// </summary>
    /// <returns></returns>
	private IEnumerator WaitAndExplode()
    {
        yield return new WaitForFixedUpdate();

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius);

        foreach (Collider2D coll in colliders)
        {
            if (coll.GetComponent<Rigidbody2D>() && coll.name != "hero")
            {
                AddExplosionForce(coll.GetComponent<Rigidbody2D>(), force, transform.position, radius, upliftModifer);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Projectile"))
        {
            DoExplosion(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 1));
        }
    }

    /// <summary>
    /// adds explosion force to given rigidbody
    /// </summary>
    /// <param name="body">rigidbody to add force to</param>
    /// <param name="explosionForce">base force of explosion</param>
    /// <param name="explosionPosition">location of the explosion source</param>
    /// <param name="explosionRadius">radius of explosion effect</param>
    /// <param name="upliftModifier">factor of additional upward force</param>
    private void AddExplosionForce(Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius, float upliftModifier = 0)
    {
        var dir = (body.transform.position - explosionPosition);
        float wearoff = 1 - (dir.magnitude / explosionRadius);
        Vector3 baseForce = dir.normalized * explosionForce * wearoff;
        baseForce.z = 0;
        body.AddForce(baseForce);

        if (upliftModifer != 0)
        {
            float upliftWearoff = 1 - upliftModifier / explosionRadius;
            Vector3 upliftForce = Vector2.up * explosionForce * upliftWearoff;
            upliftForce.z = 0;
            body.AddForce(upliftForce);
        }

    }


}
