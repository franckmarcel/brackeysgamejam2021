using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private const int CARLOS = 1, PORKCHOP_SHOWER = 2, RANDOM_OBJECT = 3;
    public Image fade, needle, death;
    public GameObject carlos;
    public float force = 250, radius = 10, upliftModifer = 5;
    public List<GameObject> objectToSpawn;
    public GameObject ground;

    Vector2 startPosition, initialPosition;
    float alpha = 0f;
    float effectCoolDown = 5f;
    float shakiraSong = 1f;

    int action = 0;
    public void Play()
    {
        alpha = 0;
        StartCoroutine(FadeOut());
    }

    public void Shake()
    {
        shakiraSong = 1;
        StartCoroutine(Shakira());
    }

    IEnumerator Shakira()
    {
        while (shakiraSong >= 0)
        {
            float dy = Random.Range(-0.2f, 0.2f);
            ground.transform.Translate(Vector2.up * dy);
            float xClamp = Mathf.Clamp(ground.transform.position.x, initialPosition.x, initialPosition.x);
            float yClamp = Mathf.Clamp(ground.transform.position.y, initialPosition.y, initialPosition.y + 5f);
            ground.transform.position = new Vector2(xClamp, yClamp);
            yield return new WaitForSeconds(0.01f);
            shakiraSong -= 0.01f;
        }
        ground.transform.position = initialPosition;
    }

    private void FixedUpdate()
    {
        Debug.Log(action);
        float interpolation = Time.deltaTime * 500;
        needle.transform.RotateAround(needle.transform.position, Vector3.forward, -interpolation);
        if (effectCoolDown <= 0)
        {
            action = Random.Range(1, 4);
            effectCoolDown = 10f;
        }
        else if (action == 0)
        {
            effectCoolDown -= Time.deltaTime;
        }

        switch (action)
        {
            case CARLOS:
                if (carlos.transform.localPosition.x <= ((RectTransform) gameObject.transform).rect.width)
                {
                    carlos.transform.Translate(Vector2.right * 0.1f);
                }
                else
                {
                    carlos.transform.position = startPosition;
                    action = 0;
                }
                break;
            case PORKCHOP_SHOWER:
                DoExplosion(new Vector2());
                action = 0;
                break;
            case RANDOM_OBJECT:
                GameObject obj = Instantiate(objectToSpawn[Random.Range(0, objectToSpawn.Count)], death.transform.position, Quaternion.identity);
                obj.GetComponent<SpriteRenderer>().sortingOrder = 10;
                action = 0;
                break;
            default:
                action = 0;
                break;
        }

        death.color = new Color(1,1,1, effectCoolDown/10);

    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Start()
    {
        fade.color = new Color(0, 0, 0, 0);
        startPosition = carlos.transform.position;
        initialPosition = ground.transform.position;
        for (int i = 0; i < 5; i++)
        {
            Instantiate(Resources.Load<GameObject>("Prefabs/Slime"), new Vector2(Random.Range(-3f, 3f), 0), Quaternion.identity);
            GameObject pig = Instantiate(Resources.Load<GameObject>("Prefabs/Pigrenade"), new Vector2(Random.Range(-3f, 3f), 0), Quaternion.identity);
            pig.transform.localScale = pig.transform.localScale * 2;
        }
    }

    IEnumerator FadeOut()
    {
        while (alpha < 1)
        {
            fade.color = new Color(0, 0, 0, alpha);
            alpha += 0.002f;
            yield return null;
        }
        SceneManager.LoadScene("Level");
    }


    /// <summary>
    /// create an explosion force
    /// </summary>
    /// <param name="position">location of the explosion</param>
    public void DoExplosion(Vector3 position)
    {
        SummonPorkchop(gameObject.transform.position);
        transform.localPosition = position;
        StartCoroutine(WaitAndExplode());
    }

    void SummonPorkchop(Vector2 position)
    {
        int rdm = Random.Range(10, 20);
        for (int i = 0; i < rdm; i++)
        {
            float dx = Random.Range(-1f, 1f);
            float dy = Random.Range(0f, 2f);
            GameObject porkchop = Instantiate(Resources.Load<GameObject>("Prefabs/Porkchop"), new Vector2(position.x + dx, position.y + dy), Quaternion.identity);
            porkchop.transform.localScale = porkchop.transform.localScale * 3;
        }
    }

    /// <summary>
    /// exerts an explosion force on all rigidbodies within the given radius
    /// </summary>
    /// <returns></returns>
	private IEnumerator WaitAndExplode()
    {
        yield return new WaitForFixedUpdate();

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius);

        foreach (Collider2D coll in colliders)
        {
            if (coll.GetComponent<Rigidbody2D>() && coll.name != "hero")
            {
                AddExplosionForce(coll.GetComponent<Rigidbody2D>(), force, transform.position, radius, upliftModifer);
            }
        }
    }

    /// <summary>
    /// adds explosion force to given rigidbody
    /// </summary>
    /// <param name="body">rigidbody to add force to</param>
    /// <param name="explosionForce">base force of explosion</param>
    /// <param name="explosionPosition">location of the explosion source</param>
    /// <param name="explosionRadius">radius of explosion effect</param>
    /// <param name="upliftModifier">factor of additional upward force</param>
    private void AddExplosionForce(Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius, float upliftModifier = 0)
    {
        var dir = (body.transform.position - explosionPosition);
        float wearoff = 1 - (dir.magnitude / explosionRadius);
        Vector3 baseForce = dir.normalized * explosionForce * wearoff;
        baseForce.z = 0;
        body.AddForce(baseForce);

        if (upliftModifer != 0)
        {
            float upliftWearoff = 1 - upliftModifier / explosionRadius;
            Vector3 upliftForce = Vector2.up * explosionForce * upliftWearoff;
            upliftForce.z = 0;
            body.AddForce(upliftForce);
        }

    }

}
