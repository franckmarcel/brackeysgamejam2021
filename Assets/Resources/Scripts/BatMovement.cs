using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatMovement : MonoBehaviour
{
    public GameObject target;
    public float offset = 0.1f;
    public float speed = 0.1f;

    private float acceleration = 1f;
    private float interpolation = 0;
    private bool follow = true;
    

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        interpolation = speed * acceleration * Vector2.Distance(target.transform.position, gameObject.transform.position);
        
        if (follow)
        {
            Follow(interpolation);
            if (acceleration < 1)
                acceleration += offset * Time.deltaTime;
        }

    }

    /// <summary>
    /// Follow the player
    /// </summary>
    void Follow(float t)
    {
        gameObject.transform.Translate((target.transform.position - gameObject.transform.position).normalized * t);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            interpolation = 0;
            follow = false;
            acceleration = 0;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            follow = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
            gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up*500);
    }

    private void OnCollisionExit(Collision collision)
    {
       
    }

}
